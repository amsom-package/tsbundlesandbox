# Changelog
<!-- introduction au changelog -->

## V1.2.0 - 23/05/2024

### Changed

- Bundle des dependances, amélioration de la gestion des cookies

## V1.1.0 - 22/05/2024

### Changed

- Ajout de la notion de token expiré et de isLogged


## V1.0.0 - 21/05/2024

### Added

- Initialisation du projet de gestion des utilisations d'AMSOM Habitat

## V0.0.0 - 05/03/2023

### Added

- blabla
- blabla

### Fixed

- blabla
- blabla

### Changed

- blabla
- blabla

### Removed

- blabla
- blabla
