module.exports = {
  mode: 'production', // Définir le mode de production pour minifier le bundle
  entry: './src/index.ts', // Fichier d'entrée principal de votre application
  output: {
    filename: 'bundle.js', // Nom du fichier de bundle final
    path: __dirname + '/dist', // Chemin d'accès au répertoire de sortie,
    library: {
      name: 'UserManager',
      type: 'umd',
      export: 'default'
    },
    globalObject: 'this'
  },
  module: {
    rules: [
      {
        test: /\.ts$/, // Charger les fichiers TypeScript
        use: {
          loader: 'ts-loader' // Utiliser le loader TypeScript
        },
        exclude: /node_modules/ // Exclure le dossier node_modules
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js'] // Spécifier les extensions de fichiers à charger
  }
};
